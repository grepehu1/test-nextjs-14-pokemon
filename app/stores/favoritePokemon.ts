import { create } from "zustand";
import { type Pokemon } from "../utils/types";

interface FavoritePokemon {
    pokemons: Pokemon[]
    togglePokemon: (pokemon: Pokemon) => void
}

const useFavoritePokemon = create<FavoritePokemon>((set) => {

    function togglePokemon(pokemon: Pokemon) {
        set(state => {
            const isIn = state.pokemons.find(thisPok => thisPok.name === pokemon.name)
            if (isIn) {
                return { pokemons: state.pokemons.filter(thisPok => thisPok.name !== pokemon.name) }
            } else {
                return { pokemons: state.pokemons.concat(pokemon) }
            }
        })
    }

    return {
        pokemons: [],
        togglePokemon,
    }
})

export { useFavoritePokemon }