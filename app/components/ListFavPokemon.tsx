'use client'

import React from 'react'
import { useFavoritePokemon } from '../stores/favoritePokemon'
import Link from 'next/link'

export default function ListFavPokemon() {
    const pokemons = useFavoritePokemon(state => state.pokemons)

    return (
        <ul>
            {pokemons?.map(pokemon => {
                return (
                    <li key={`list-pokemon-item-${pokemon.name}`}>
                        <Link href={`/${pokemon.name}`}>
                            {pokemon.name}
                        </Link>
                    </li>
                )
            })}
        </ul>
    )
}
