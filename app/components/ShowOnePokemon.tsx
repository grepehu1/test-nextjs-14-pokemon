'use client'

import { useFavoritePokemon } from "../stores/favoritePokemon"
import { type Pokemon } from "../utils/types"

export default function ShowOnePokemon({ pokemon }: { pokemon: Pokemon }) {
    const favPokemons = useFavoritePokemon(state => state.pokemons)
    const togglePokemon = useFavoritePokemon(state => state.togglePokemon)

    return (
        <>
            <div>{pokemon.name}</div>
            <button onClick={() => togglePokemon(pokemon)}>
                {favPokemons.find(thisPok => thisPok.name === pokemon.name) ? (
                    <>Remove From Favs</>
                ) : (
                    <>Add to Favs</>
                )}
            </button>
        </>
    )
}
