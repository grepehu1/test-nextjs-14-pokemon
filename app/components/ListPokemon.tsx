'use client'

import Link from "next/link"
import usePokemon from "../hooks/usePokemon"

export default function ListPokemon() {
    const { pokemons, getMorePokemon } = usePokemon()

    return (
        <>
            <ul>
                {pokemons?.map(pokemon => {
                    return (
                        <li key={`list-pokemon-item-${pokemon.name}`}>
                            <Link href={`/${pokemon.name}`}>
                                {pokemon.name}
                            </Link>
                        </li>
                    )
                })}
            </ul>
            <button onClick={getMorePokemon}>More</button>
        </>
    )
}
