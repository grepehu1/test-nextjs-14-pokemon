import { fetchSinglePokemon } from "../api/pokemon"
import ShowOnePokemon from "../components/ShowOnePokemon"

async function getPokePage(pokeId: string) {
    const pokemon = await fetchSinglePokemon(pokeId)

    return pokemon
}

export default async function PokemonPage({
    params
}: {
    params: { pokeId: string }
}) {
    const data = await getPokePage(params.pokeId)

    return (
        <ShowOnePokemon pokemon={data} />
    )
}
