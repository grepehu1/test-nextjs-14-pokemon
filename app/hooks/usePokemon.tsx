'use client'

import { useCallback, useEffect, useState } from "react"
import { fetchAllPokemon } from "../api/pokemon"
import { type Pokemon } from "../utils/types"

export default function usePokemon() {
    const [pokemons, setPokemons] = useState<Pokemon[]>([])

    const getMorePokemon = useCallback(
        async () => {
            const data = await fetchAllPokemon(pokemons.length)
            setPokemons(prev => prev.concat(data.results))
        },
        [pokemons],
    )


    useEffect(() => {
        getMorePokemon()
    }, []) // eslint-disable-line


    return { pokemons, getMorePokemon }
}
