import { type Pokemon } from "../utils/types";

interface PokemonListResponse {
    count: number;
    results: Pokemon;
}

export async function fetchAllPokemon(offset = 0, limit = 20) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_POKEMON_URL}/pokemon?limit=${limit}&offset=${offset}`)
    const data = await res.json() as PokemonListResponse

    return data
}

export async function fetchSinglePokemon(pokeId: string) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_POKEMON_URL}/pokemon/${pokeId}`)
    const data = await res.json() as Pokemon

    return data
}