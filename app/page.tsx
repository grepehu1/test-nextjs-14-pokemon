import Image from 'next/image'
import ListPokemon from './components/ListPokemon'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <ListPokemon />
    </main>
  )
}
